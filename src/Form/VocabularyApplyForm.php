<?php

/**
 * @file
 * Contains \Drupal\taxonomy_apply\Form\VocabularyApplyForm.
 */

namespace Drupal\taxonomy_apply\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form for applying vocabularies to entity bundles.
 */
class VocabularyApplyForm extends EntityForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityBundleInfo;

  /**
   * The query factory to create entity queries.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $queryFactory;

  /**
   * Constructs a VocabularyApplyForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_bundle_info
   *   The entity bundle info service.
   * @param \Drupal\Core\Entity\Query\QueryFactory $query_factory
   *   The entity query object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_bundle_info, QueryFactory $query_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityBundleInfo = $entity_bundle_info;
    $this->queryFactory = $query_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_apply_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['cardinality'] = [
      '#type' => 'radios',
      '#title' => t('Cardinality'),
      '#description' => t("How many terms of this vocabulary may be selected for a particular entity"),
      '#options' => [
        1 => t('Single value'),
        FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED => t('Multiple values'),
      ],
    ];

    // Figure which bundles already have this vocabulary applied.
    // Get reference fields which point to taxonomy terms.
    $field_storage_ids = $this->queryFactory->get('field_storage_config')
      ->condition('type', 'entity_reference')
      ->condition('settings.target_type', 'taxonomy_term')
      ->execute();

    //dsm($field_storage_ids);


    // The storage IDs are in the form ENTITYTYPE.FIELDNAME, so hack that rather
    // than have to load them all. ???
    $field_names = [];
    foreach ($field_storage_ids as $field_storage_id) {
      list(, $field_names[]) = explode('.', $field_storage_id);
    }

    $field_ids = $this->queryFactory->get('field_config')
      ->condition('field_name', $field_names, 'IN')
      ->execute();

    $fields = FieldConfig::loadMultiple($field_ids);
    //dsm($fields);

    // Get the reference fields which point to this vocabulary.
    $relevant_fields = [];
    foreach ($fields as $field_id => $field) {
      $target_bundles = $field_settings = $field->getSettings()['handler_settings']['target_bundles'];

      // Don't deal with reference fields that point to more than one vocab.
      if (count($target_bundles) > 1) {
        continue;
      }

      if (in_array($this->entity->id(), $target_bundles)) {
        $key = $field->get('entity_type') . ':' . $field->get('bundle');

        $relevant_fields[$key] = $field;
      }
    }
    //dsm($relevant_fields);

    // Assemble options for all content entity types and their bundles.
    $options = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type => $entity_type_definition) {
      if (!($entity_type_definition instanceof ContentEntityType)) {
        continue;
      }

      // Skip taxonomy itself.
      if ($entity_type == 'taxonomy_term') {
        continue;
      }

      $bundles = $this->entityBundleInfo->getBundleInfo($entity_type);

      foreach ($bundles as $bundle_id => $bundle_label) {
        $options[$entity_type . ':' . $bundle_id] = t("@entity: @bundle", [
          '@entity' => $entity_type_definition->getLabel(),
          '@bundle' => $bundle_label['label'],
        ]);
      }
    }
    // Sort the options by label.
    asort($options, SORT_FLAG_CASE);

    $existing_entity_bundles = array_keys($relevant_fields);

    $form['bundles'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => array_combine($existing_entity_bundles, $existing_entity_bundles),
      '#description' => t("Select bundles on which to apply this vocabulary's term reference field."),
    );

    // Disable checkboxes for bundles which already have the field.
    foreach ($existing_entity_bundles as $key) {
      $form['bundles'][$key]['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply vocabularies'),
    ];
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $vocabulary_name = $this->entity->id();

    $bundles = array_filter($values['bundles']);
    // TODO: filter out the ones that already have an existing field.

    $fields_to_create = [];
    foreach ($bundles as $key) {
      list($entity_type_id, $bundle_id) = explode(':', $key);
      $fields_to_create[$entity_type_id][] = $bundle_id;
    }

    $field_name = 'field_' . $vocabulary_name;

    foreach ($fields_to_create as $entity_type_id => $bundle_ids) {
      // Create field storage if it doesn't already exist on this entity type.
      $field_storage_ids = $this->queryFactory->get('field_storage_config')
        ->condition('field_name', $field_name)
        ->condition('entity_type', $entity_type_id)
        ->execute();

      if (empty($field_storage_ids)) {
        $field_storage = FieldStorageConfig::create(array(
          'field_name' => $field_name,
          'entity_type' => $entity_type_id,
          'type' => 'entity_reference',
          'translatable' => FALSE,
          'entity_types' => array(),
          'settings' => array(
            'target_type' => 'taxonomy_term',
          ),
          'cardinality' => $values['cardinality'],
        ));
        $field_storage->save();
      }
      else {
        $field_storage_id = array_pop($field_storage_ids);
        $field_storage = FieldStorageConfig::load($field_storage_id);
      }

      // Create a field for each bundle.
      foreach ($bundle_ids as $bundle_id) {
        $field = FieldConfig::create([
          'label' => $this->entity->label(),
          'field_storage' => $field_storage,
          'entity_type' => $entity_type_id,
          'bundle' => $bundle_id,
          'settings' => array(
            'handler' => 'default',
            'handler_settings' => array(
              // Reference a single vocabulary.
              'target_bundles' => array(
                $vocabulary_name,
              ),
              // TODO: Enable auto-create.
              //'auto_create' => TRUE,
            ),
          ),
        ]);
        $field->save();

        // Update the form display to show the field.
        $form_display = $this->entityTypeManager->getStorage('entity_form_display')->load($entity_type_id . '.' . $bundle_id . '.' . 'default');

        $options = [
          // TODO: depends on settings.
          'type' => 'options_select',
          // TODO: weight
        ];
        $form_display->setComponent($field_name, $options);
        $form_display->save();
      }
    }

    drupal_set_message(t("The fields have been created."));
  }

}
