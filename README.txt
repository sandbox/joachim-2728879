Taxonomy Apply
==============

Provides a UI to create taxonomy term reference fields from within the
taxonomy vocabulary admin UI.

See this core issue: https://www.drupal.org/node/889378 for background.
